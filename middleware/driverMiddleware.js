const User = require('../models/User');

module.exports = async function(req, res, next) {
  try {
    const user = await User.findOne({_id: req.user.id});
    if (user.role != 'DRIVER') {
      return res.status(400).send({message: 'User is not driver'});
    }
    next();
  } catch (error) {
    console.error(error);
    return res.status(400).send('Error message');
  }
};
