const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport(
    {
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: 'nodejs.hw3@gmail.com',
        pass: 'qQ12345aA',
      },
      tls: {
        rejectUnauthorized: false,
      },
    },
    {
      from: 'NodeJS hw3 <nodejs.hw3@gmail.com>',
    },
);

const mailer = (message) =>{
  transporter.sendMail(message, (err, info)=>{
    if (err) {
      console.error(err);
    }
  });
};

module.exports = mailer;
