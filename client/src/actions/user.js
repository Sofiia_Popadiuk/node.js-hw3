import axios from 'axios'
import { setUser } from '../reducers/userReducer'

export const registration = async (email, password, role) => {
    try {
        const response = await axios.post('http://localhost:8080/api/auth/register', {
            email, password, role
        })
        alert(response.data.message + '\nPlease login now')
    } catch (error) {
        alert(error?.response?.data)
    }
}

export const login = (email, password) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8080/api/auth/login', {
                email, password
            })
            localStorage.setItem('token', response.data.jwt_token)
            dispatch(getUser())
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}

export const getUser = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8080/api/users/me',
                { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            dispatch(setUser(response.data.user))
            sessionStorage.setItem('role', response.data.user.role)
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}

export const forgotPassword = async (email) => {
    try {
        await axios.post('http://localhost:8080/api/auth/forgot_password', {
            email
        })
        alert(`New password sent on ${email}`)
    } catch (error) {
        alert(error?.response?.data)
    }
}