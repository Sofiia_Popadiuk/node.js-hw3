import axios from 'axios'
import { setCreatedTrucks, setAssignedLoad } from '../reducers/driverReducer'

export const addTruck = (type) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8080/api/trucks', {
                type: type
            }, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            alert(response?.data?.message)
        } catch (error) {
            alert(error?.response?.data)
        }
    }
}

export const getCreatedTrucks = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8080/api/trucks',
                { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            dispatch(setCreatedTrucks(response.data.trucks))
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}

export const getAssignedLoad = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8080/api/loads/active',
                { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            dispatch(setAssignedLoad(response.data.load))
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}

// export const iterateToNextStage = () => {
//     return async dispatch => {
//         try {
//             await axios.patch('http://localhost:8080/api/loads/active/state',
//             {headers:{Authorization: `Bearer ${localStorage.getItem('token')}`}})
//             dispatch(setAssignedLoad({}))
//         } catch (error) { 
//             alert(error?.response?.data?.message)
//         }
//     }
// }