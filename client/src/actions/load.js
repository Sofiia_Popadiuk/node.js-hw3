import axios from 'axios'
import { setAvailableLoad, setLoadForMoreInfo } from '../reducers/shipperReducer'

export const addLoad = async (name, payload, pickup, delivery, width, length, height) => {
    try {
        const response = await axios.post('http://localhost:8080/api/loads', {
            name, payload,
            pickup_address: pickup,
            delivery_address: delivery,
            dimensions: {
                width, length, height
            }
        }, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
        alert(response?.data?.message)
    } catch (error) {
        alert(error?.response?.data)
    }
}

export const getAvailableLoad = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8080/api/loads',
                { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            dispatch(setAvailableLoad(response.data.loads))
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}

export const getLoadById = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get(`http://localhost:8080/api/loads/${id}`,
                { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
            dispatch(setLoadForMoreInfo(response.data.load))
        } catch (error) {
            alert(error?.response?.data?.message)
        }
    }
}
