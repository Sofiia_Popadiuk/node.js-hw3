import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { getUser } from './actions/user';
import { getCreatedTrucks, getAssignedLoad } from './actions/truck';
import { getAvailableLoad, getLoadById } from './actions/load';
import AddLoad from './components/shipper/AddLoad/AddLoad';
import AddTruck from './components/driver/AddTruck/AddTruck';
import Login from './components/Login/Login';
import NewLoads from './components/shipper/NewLoads/NewLoads';
import Register from './components/Register/Register';
import CreatedTrucks from './components/driver/CrearedTrucks/CreatedTrucks';
import ActiveLoad from './components/driver/ActiveLoad/ActiveLoad';
import Profile from './components/Profile/Profile';
import ForgotPassword from './components/ForgotPassword/ForgotPassword';
import AssignedLoad from './components/shipper/AssignedLoad/AssignedLoad'
import History from './components/shipper/History/History'
import MoreInfo from './components/shipper/MoreInfo/MoreInfo';
import DriverHistory from './components/driver/History/History'

function App() {
  const dispatch = useDispatch()
  const role = sessionStorage.getItem('role')

  useEffect(() => {
    if (window.location.href !== 'http://localhost:3000/'
      && window.location.href !== 'http://localhost:3000/login'
      && window.location.href !== 'http://localhost:3000/forgotPassword') {
      dispatch(getUser())
      if (role === 'DRIVER') {
        dispatch(getCreatedTrucks())
        dispatch(getAssignedLoad())
        dispatch(getAvailableLoad())
      } else {
        dispatch(getAvailableLoad())
        localStorage.getItem('id') && dispatch(getLoadById(localStorage.getItem('id')))
      }
    }
  }, [])

  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path='/' component={Register} />
          <Route path='/login' component={Login} />
          <Route path='/newLoads' component={NewLoads} />
          <Route path='/addNewLoad' component={AddLoad} />
          <Route path='/addNewTruck' component={AddTruck} />
          <Route path='/createdTrucks' component={CreatedTrucks} />
          <Route path='/activeLoad' component={ActiveLoad} />
          <Route path='/profile' component={Profile} />
          <Route path='/forgotPassword' component={ForgotPassword} />
          <Route path='/assignedLoads' component={AssignedLoad} />
          <Route path='/history' component={History} />
          <Route path='/moreInfo/:id' component={MoreInfo} />
          <Route path='/driverHistory' component={DriverHistory} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
