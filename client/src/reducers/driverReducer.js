const SET_CREATED_TRUCKS = "SET_CREATED_TRUCKS"
const GET_ASSIGNED_LOAD = "GET_ASSIGNED_LOAD"

const defaultState = {
    createdTrucks: [],
    assignedLoad: {}
}

export default function driverReducer(state = defaultState, action) {
    switch (action.type) {
        case SET_CREATED_TRUCKS:
            return {
                ...state,
                createdTrucks: action.payload
            }
        case GET_ASSIGNED_LOAD:
            return {
                ...state,
                assignedLoad: action.payload
            }
        default:
            return state
    }
}

export const setCreatedTrucks = trucks => ({ type: SET_CREATED_TRUCKS, payload: trucks })
export const setAssignedLoad = load => ({ type: GET_ASSIGNED_LOAD, payload: load })