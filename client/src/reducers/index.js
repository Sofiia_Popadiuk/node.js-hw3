import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import userReducer from './userReducer'
import driverReducer from './driverReducer'
import shipperReducer from './shipperReducer'

const rootReducer = combineReducers({
    user: userReducer,
    trucks: driverReducer,
    loads: shipperReducer
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))