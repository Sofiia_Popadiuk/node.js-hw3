const SET_AVAILABLE_LOAD = "SET_AVAILABLE_LOAD"
const SET_LOAD_FOR_MORE_INFO = "SET_LOAD_FOR_MORE_INFO"

const defaultState = {
    availableLoad: [],
    loadForMoreInfo: {},
}

export default function shipperReducer(state = defaultState, action) {
    switch (action.type) {
        case SET_AVAILABLE_LOAD:
            return {
                ...state,
                availableLoad: action.payload,
            }
        case SET_LOAD_FOR_MORE_INFO:
            return {
                ...state,
                loadForMoreInfo: action.payload,
            }
        default:
            return state
    }
}

export const setAvailableLoad = load => ({ type: SET_AVAILABLE_LOAD, payload: load })
export const setLoadForMoreInfo = load => ({ type: SET_LOAD_FOR_MORE_INFO, payload: load })