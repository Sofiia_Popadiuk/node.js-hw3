import React from "react";
import styles from './MyInput.module.css'

const MyInput = (props) => {
    return (
        // <input type={props.type} placeholder={props.placeholder} className={styles.input} />
        <input {...props} className={styles.input} />
    )
}

export default MyInput