import React from "react";
import { useSelector } from "react-redux";
import Header from "../../Header/Header";
import Nav from "../Nav/Nav";
import styles from './AssignedLoad.module.css'

const AssignedLoad = () => {
    const loads = useSelector(state => state.loads.availableLoad)
    const assignedLoad = loads.filter(load => load.status === 'ASSIGNED')

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    <h2>Assigned loads:</h2>
                    <table className={styles.table_dark}>
                        <thead>
                            <tr>
                                <th>NAME</th>
                                <th>PICKUP ADRESS</th>
                                <th>DELIVERY ADRESS</th>
                                <th>CREATED DATE</th>
                                <th>DRIVER ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            {assignedLoad.map(load =>
                                <tr key={load._id}>
                                    <td>{load.name}</td>
                                    <td>{load?.pickup_address}</td>
                                    <td>{load?.delivery_address}</td>
                                    <td>{load?.created_date}</td>
                                    <td>{load?.assigned_to}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default AssignedLoad