import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../Header/Header";
import Nav from "../Nav/Nav";
import styles from './NewLoads.module.css'
import showMore from '../../../images/showMore.png'
import edit from '../../../images/edit.png'
import { getLoadById } from '../../../actions/load'
import { useHistory } from "react-router-dom";

const NewLoads = () => {
    const loads = useSelector(state => state.loads.availableLoad)
    const dispatch = useDispatch()
    const newLoad = loads.filter(load => load.status === 'NEW')
    const history = useHistory()

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    <h2>New loads:</h2>
                    <table className={styles.table_dark}>
                        <thead>
                            <tr>
                                <th>NAME</th>
                                <th>PICKUP ADRESS</th>
                                <th>DELIVERY ADRESS</th>
                                <th>CREATED DATE</th>
                                <th>ADDITIONAL/EDIT</th>
                            </tr>
                        </thead>
                        <tbody>
                            {newLoad.map(load =>
                                <tr key={load._id} id={load._id}>
                                    <td>{load.name}</td>
                                    <td>{load?.pickup_address}</td>
                                    <td>{load?.delivery_address}</td>
                                    <td>{load?.created_date}</td>
                                    <td className={styles.bg}>
                                        <div className={styles.flex}>
                                            <img onClick={event => {
                                                event.preventDefault()
                                                dispatch(getLoadById(event.target.parentNode.parentNode.parentNode.id))
                                                localStorage.setItem('id', event.target.parentNode.parentNode.parentNode.id)
                                                history.push(`/moreInfo/${event.target.parentNode.parentNode.parentNode.id}`)
                                            }} className={styles.image} src={showMore} alt="Show more" />
                                            <img className={styles.image} src={edit} alt="Edit" />
                                        </div>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default NewLoads