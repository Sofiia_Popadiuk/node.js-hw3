import React, { useState } from "react";
import Header from "../../Header/Header";
import MyButton from "../../MyButton/MyButton";
import Nav from "../Nav/Nav";
import styles from './AddLoad.module.css'
import { addLoad } from '../../../actions/load'

const AddLoad = () => {
    const [name, setName] = useState('')
    const [payload, setPayload] = useState('')
    const [pickup, setPickup] = useState('')
    const [delivery, setDelivery] = useState('')
    const [width, setWidth] = useState('')
    const [length, setLength] = useState('')
    const [height, setHeight] = useState('')

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <form onSubmit={() => {
                    addLoad(name, payload, pickup, delivery, width, length, height)
                }} className={styles.mainPart}>
                    <h2>Add new load</h2>
                    <input onChange={event => setName(event.target.value)} className={styles.bigInput} type="text" placeholder="Name of load" />
                    <input onChange={event => setPayload(event.target.value)} className={styles.bigInput} type="text" placeholder="Payload" />
                    <input onChange={event => setPickup(event.target.value)} className={styles.bigInput} type="text" placeholder="Pickup address" />
                    <input onChange={event => setDelivery(event.target.value)} className={styles.bigInput} type="text" placeholder="Delivery address" />
                    <h3 className={styles.h3}>Dimensions</h3>
                    <input onChange={event => setWidth(event.target.value)} className={styles.smallInput} type="text" placeholder="Width" />
                    <input onChange={event => setLength(event.target.value)} className={styles.smallInput} type="text" placeholder="Length" />
                    <input onChange={event => setHeight(event.target.value)} className={styles.smallInput} type="text" placeholder="Height" />
                    <MyButton type="submit" style={{ "padding": "0 70px" }}>Add load</MyButton>
                </form>
            </div>
        </div>
    )
}

export default AddLoad