import React from "react";
import styles from './Nav.module.css'
import { useHistory } from 'react-router-dom'

const Nav = () => {
    const history = useHistory();

    const redirectToAddNewLoad = event => {
        event.preventDefault();
        history.push("/addNewLoad");
    }

    const redirectToNewLoads = event => {
        event.preventDefault();
        history.push("/newLoads");
    }

    const redirectToAssignedLoads = event => {
        event.preventDefault();
        history.push("/assignedLoads");
    }

    const redirectToHistory = event => {
        event.preventDefault();
        history.push("/history");
    }

    const redirectToProfile = event => {
        event.preventDefault();
        history.push("/profile");
    }

    return (
        <div className={styles.nav}>
            <h2 className={styles.h2}>Shipper page</h2>
            <div className={styles.point} onClick={event => redirectToAddNewLoad(event)}>Add new load</div>
            <div className={styles.point} onClick={event => redirectToNewLoads(event)}>New Loads</div>
            <div className={styles.point} onClick={event => redirectToAssignedLoads(event)}>Assigned Loads</div>
            <div className={styles.point} onClick={event => redirectToHistory(event)}>History</div>
            <div className={styles.point} onClick={event => redirectToProfile(event)}>Profile</div>
        </div>
    )
}

export default Nav