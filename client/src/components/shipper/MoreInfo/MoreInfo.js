import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../Header/Header";
import Nav from "../Nav/Nav";
import { getLoadById } from '../../../actions/load'
import styles from './MoreInfo.module.css'

const MoreInfo = () => {
    const dispatch = useDispatch()
    
    const load = useSelector(state => state.loads.loadForMoreInfo)
    let keys;
    if (load) {
        keys = Object.keys(load)
    }
    console.log(load)
    console.log(keys)
    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    {
                        load ?
                            <div>
                                <h2>{`All information about load ${load._id}:`}</h2>
                                <table className={styles.table_dark}>
                                    <thead>
                                        <tr>
                                            <th>PARAMETR</th>
                                            <th>VALUR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {keys.map(key =>
                                            <tr key={key}>
                                                <td>{key}</td>
                                                <td>
                                                    {
                                                        load[key] ?
                                                            load[key].toString() === '[object Object]' ?
                                                                Object.entries(load[key]).toString() :
                                                                load[key] === 'logs' ?
                                                                    load[key].map(obj =>
                                                                        <p key={obj.time}>{`Message: ${obj.message}, time: ${obj.time}`}</p>
                                                                    ) :
                                                                    load[key]
                                                            : '-'
                                                    }
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div> :
                            <h2>No assigned load yet!</h2>
                    }
                </div>
            </div>
        </div>
    )
}

export default MoreInfo