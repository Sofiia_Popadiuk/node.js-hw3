import React, { useState } from 'react'
import MyInput from '../MyInput/MyInput'
import MyButton from '../MyButton/MyButton'
import styles from './ForgotPassword.module.css'
import { useHistory } from 'react-router-dom'
import { forgotPassword } from '../../actions/user'

import backPhoto from '../../images/back.png'

const app = {
    background: `url(${backPhoto}`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
}

const ForgotPassword = () => {
    let history = useHistory();
    const [email, setEmail] = useState('')

    const redirectToLogin = event => {
        event.preventDefault();
        history.push("/login");
    };

    return (
        <div className={styles.page} style={app}>
            <form className={styles.form}>
                <h1 className={styles.h1}>Forgot Password</h1>
                <MyInput value={email} onChange={e => setEmail(e.target.value)} type="text" placeholder="Email" />

                <MyButton onClick={event => {
                    event.preventDefault()
                    forgotPassword(email)
                }} style={{ "marginRight": "10px" }}>Send new password</MyButton>
                <MyButton onClick={event => redirectToLogin(event)}>Go to login</MyButton>
            </form>
        </div>
    )
}

export default ForgotPassword