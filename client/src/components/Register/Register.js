import React, { useState } from 'react'
import MyInput from '../MyInput/MyInput'
import MyButton from '../MyButton/MyButton'
import styles from './Register.module.css'
import { useHistory } from 'react-router-dom'
import { registration } from '../../actions/user'

import backPhoto from '../../images/back.png'

const app = {
    background: `url(${backPhoto}`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
}

const Register = () => {
    let history = useHistory();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('')

    const redirectToLogin = event => {
        event.preventDefault();
        history.push("/login");
    };

    return (
        <div className={styles.page} style={app}>
            <form className={styles.form}>
                <h1 className={styles.h1}>Registration</h1>
                <MyInput value={email} onChange={e => setEmail(e.target.value)} type="text" placeholder="Email" />
                <MyInput value={password} onChange={e => setPassword(e.target.value)} type="text" placeholder="Password" />

                <h3 className={styles.h3}>Choose your role</h3>
                <div className={styles.role}>
                    <div>
                        <input type="radio" id="shipper" name="role" value="SHIPPER" onChange={event => setRole(event.target.value)} />
                        <label htmlFor="shipper">Shipper</label>
                    </div>
                    <div>
                        <input type="radio" id="driver" name="role" value="DRIVER" onChange={event => setRole(event.target.value)} />
                        <label htmlFor="driver">Driver</label>
                    </div>
                </div>

                <MyButton onClick={event => {
                    event.preventDefault()
                    registration(email, password, role)
                }} style={{ "marginRight": "10px" }}>Register</MyButton>
                <MyButton onClick={event => redirectToLogin(event)}>Go to login</MyButton>
            </form>
        </div>
    )
}

export default Register