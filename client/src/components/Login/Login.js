import React, { useEffect, useState } from 'react'
import MyInput from '../MyInput/MyInput'
import MyButton from '../MyButton/MyButton'
import styles from './Login.module.css'
import { useHistory } from 'react-router-dom'
import { login } from '../../actions/user'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux';
import { getCreatedTrucks, getAssignedLoad } from '../../actions/truck';
import { getAvailableLoad, getLoadById } from '../../actions/load';

import backPhoto from '../../images/back.png'

const app = {
    background: `url(${backPhoto}`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center'
}

const Login = () => {
    let history = useHistory();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch();
    const isAuth = useSelector(state => state.user.isAuth)
    const roleShipper = useSelector(state => state.user.currentUser.role === 'SHIPPER')

    useEffect(() => {
        isAuth && roleShipper &&
            dispatch(getAvailableLoad()) &&
            history.push("/addNewLoad");
        isAuth && !roleShipper &&
            dispatch(getCreatedTrucks()) &&
            dispatch(getAssignedLoad()) &&
            dispatch(getAvailableLoad()) &&
            history.push("/addNewTruck");
    }, [isAuth])

    const redirectToRegister = event => {
        event.preventDefault();
        history.push("/");
    };

    const redirectToForgotPassword = event => {
        event.preventDefault();
        history.push("/forgotPassword");
    };

    return (
        <div className={styles.page} style={app}>
            <form className={styles.form}>
                <h1 className={styles.h1}>Login</h1>
                <MyInput value={email} onChange={e => setEmail(e.target.value)} type="text" placeholder="Email" />
                <MyInput value={password} onChange={e => setPassword(e.target.value)} type="text" placeholder="Password" />

                <MyButton onClick={event => {
                    event.preventDefault()
                    dispatch(login(email, password))
                }} style={{ "marginRight": "10px" }}>Login</MyButton>
                <MyButton onClick={event => redirectToForgotPassword(event)} style={{ "marginRight": "10px" }}>Forgot password</MyButton>
                <MyButton onClick={event => redirectToRegister(event)}>Go to registration</MyButton>
            </form>
        </div>
    )
}

export default Login