import React from "react";
import styles from './Nav.module.css'
import { useHistory } from 'react-router-dom'

const Nav = () => {
    const history = useHistory();

    const redirectToAddNewTruck = event => {
        event.preventDefault();
        history.push("/addNewTruck");
    }

    const redirectToCreatedTrucks = event => {
        event.preventDefault();
        history.push("/createdTrucks");
    }

    const redirectToActiveLoad = event => {
        event.preventDefault();
        history.push("/activeLoad");
    }

    const redirectToHistory = event => {
        event.preventDefault();
        history.push("/driverHistory");
    }

    const redirectToProfile = event => {
        event.preventDefault();
        history.push("/profile");
    }

    return (
        <div className={styles.nav}>
            <h2 className={styles.h2}>Driver page</h2>
            <div className={styles.point} onClick={event => redirectToAddNewTruck(event)}>Add new truck</div>
            <div className={styles.point} onClick={event => redirectToCreatedTrucks(event)}>My created trucks</div>
            <div className={styles.point} onClick={event => redirectToActiveLoad(event)}>Active load</div>
            <div className={styles.point} onClick={event => redirectToHistory(event)}>History</div>
            <div className={styles.point} onClick={event => redirectToProfile(event)}>Profile</div>
        </div>
    )
}

export default Nav