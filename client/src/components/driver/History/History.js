import React from "react";
import { useSelector } from "react-redux";
import Header from "../../Header/Header";
import Nav from "../Nav/Nav";
import styles from './History.module.css'

const History = () => {
    const loads = useSelector(state => state.loads.availableLoad)
    const oldLoad = loads.filter(load => load.status === 'SHIPPED')

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    {
                        oldLoad.length > 0 ?
                            <div>
                                <h2>History:</h2>
                                <table className={styles.table_dark}>
                                    <thead>
                                        <tr>
                                            <th>NAME</th>
                                            <th>PICKUP ADRESS</th>
                                            <th>DELIVERY ADRESS</th>
                                            <th>CREATED DATE</th>
                                            <th>DRIVER ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {oldLoad.map(load =>
                                            <tr key={load._id}>
                                                <td>{load.name}</td>
                                                <td>{load?.pickup_address}</td>
                                                <td>{load?.delivery_address}</td>
                                                <td>{load?.created_date}</td>
                                                <td>{load?.assigned_to}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>:
                            <h2>No history yet!</h2>
                   }
                </div>
            </div>
        </div>
    )
}

export default History