import React from "react";
import { useSelector } from "react-redux";
import Header from "../../Header/Header";
import Nav from "../Nav/Nav";
import styles from './CreatedTrucks.module.css'

const CreatedTrucks = () => {
    const trucks = useSelector(state => state.trucks.createdTrucks)

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    <h2>Your created trucks:</h2>
                    <table className={styles.table_dark}>
                        <thead>
                            <tr>
                                <th>TYPE</th>
                                <th>STATUS</th>
                                <th>CREATED DATE</th>
                            </tr>
                        </thead>
                       <tbody>
                       {trucks.map(truck =>
                            <tr key={truck._id}>
                                <td>{truck.type}</td>
                                <td>{truck?.status}</td>
                                <td>{truck?.created_date}</td>
                            </tr>
                        )}
                       </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default CreatedTrucks