import React, { useState } from "react";
import Header from "../../Header/Header";
import MyButton from "../../MyButton/MyButton";
import Nav from "../Nav/Nav";
import styles from './AddTruck.module.css'
import { addTruck } from '../../../actions/truck'

import smallTruck from '../../../images/smallTruck.png'
import mediumTruck from '../../../images/mediumTruck.png'
import bigTruck from '../../../images/bigTruck.png'
import { useDispatch } from "react-redux";

const AddTruck = () => {
    const [type, setType] = useState('')
    const dispatch = useDispatch();

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <form onSubmit={event => {
                    // event.preventDefault()
                    dispatch(addTruck(type))
                }} className={styles.mainPart}>
                    <h2>Add new truck</h2>
                    <div className={styles.block}>
                        <input type="radio" id="SPRINTER" name="role" value="SPRINTER" onChange={event => setType(event.target.value)} />
                        <label className={styles.input} htmlFor="SPRINTER">SPRINTER</label>
                        <p>Dimensions: 300*250*170</p>
                        <p>Payload: 1700</p>
                    </div>
                    <div className={styles.block}>
                        <input type="radio" id="SMALL_STRAIGHT" name="role" value="SMALL STRAIGHT" onChange={event => setType(event.target.value)} />
                        <label className={styles.input} htmlFor="SMALL_STRAIGHT">SMALL STRAIGHT</label>
                        <p>Dimensions: 500*250*170</p>
                        <p>Payload: 2500</p>
                    </div>
                    <div className={styles.block}>
                        <input type="radio" id="LARGE_STRAIGHT" name="role" value="LARGE STRAIGHT" onChange={event => setType(event.target.value)} />
                        <label className={styles.input} htmlFor="LARGE_STRAIGHT">LARGE STRAIGHT</label>
                        <p>Dimensions: 700*350*200</p>
                        <p>Payload: 4000</p>
                    </div>
                    <MyButton type="submit" style={{ "padding": "0 70px" }}>Add Truck</MyButton>
                </form>
                <div className={styles.pictures}>
                    {type === 'SPRINTER' && <img className={styles.small} src={smallTruck} alt="Small truck" />}
                    {type === 'SMALL STRAIGHT' && <img className={styles.medium} src={mediumTruck} alt="Medium truck" />}
                    {type === 'LARGE STRAIGHT' && <img className={styles.big} src={bigTruck} alt="Large truck" />}
                </div>
            </div>
        </div>
    )
}

export default AddTruck