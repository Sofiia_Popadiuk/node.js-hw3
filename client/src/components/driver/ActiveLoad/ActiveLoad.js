import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../Header/Header";
import MyButton from "../../MyButton/MyButton";
import Nav from "../Nav/Nav";
import styles from './ActiveLoad.module.css'
import { iterateToNextStage } from '../../../actions/truck'

const ActiveLoad = () => {
    const load = useSelector(state => state.trucks.assignedLoad)
    let keys;
    if(load){
        keys = Object.keys(load)
    }
    const dispatch = useDispatch()

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                <Nav />
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    {
                        load ?
                            <div>
                                <h2>Assigned load:</h2>
                                <table className={styles.table_dark}>
                                    <thead>
                                        <tr>
                                            <th>PARAMETR</th>
                                            <th>VALUR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {keys.map(key =>
                                            <tr key={key}>
                                                <td>{key}</td>
                                                <td>{load[key].toString() === '[object Object]' ?
                                                    Object.entries(load[key]).toString() :
                                                    Array.isArray(load[key]) ?
                                                        load[key].map(obj =>
                                                            <p key={obj.time}>{`Message: ${obj.message}, time: ${obj.time}`}</p>
                                                        ) :
                                                        load[key]}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                                <MyButton 
                                // onClick={() => dispatch(iterateToNextStage())} 
                                style={{ "marginBottom": "40px" }}>Iterate to state "Arrived to delivery"</MyButton>
                            </div> :
                            <h2>No assigned load yet!</h2>
                    }
                </div>
            </div>
        </div>
    )
}

export default ActiveLoad