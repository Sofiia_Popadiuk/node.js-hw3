import React from "react";
import { useSelector } from "react-redux";
import Header from "../Header/Header";
import DriverNav from "../driver/Nav/Nav";
import ShipperNav from "../shipper/Nav/Nav";
import styles from './Profile.module.css'

const Profile = () => {
    const user = useSelector(state => state.user.currentUser)

    return (
        <div>
            <Header />
            <div className={styles.pageContent}>
                {user.role === 'DRIVER' ? <DriverNav /> : <ShipperNav />}
                <div className={styles.w20} />
                <div className={styles.mainPart}>
                    <h2 className={styles.point}>Email:
                        <span className={styles.lighter}>{`  ${user.email}`}</span>
                    </h2>
                    <h2 className={styles.point}>Role:
                        <span className={styles.lighter}>{`  ${user.role}`}</span>
                    </h2>
                    <h2 className={styles.point}>Created date:
                        <span className={styles.lighter}>{`  ${user.created_date}`}</span>
                    </h2>
                </div>
            </div>
        </div>
    )
}

export default Profile