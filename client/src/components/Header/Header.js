import React from "react";
import styles from './Header.module.css'
import logo from '../../images/logo.png'
import { useDispatch, useSelector } from 'react-redux';
import MyButton from '../MyButton/MyButton'
import { useHistory } from 'react-router-dom'
import { logout } from "../../reducers/userReducer";

const Header = () => {
    const email = useSelector(state => state.user.currentUser?.email)
    const dispatch = useDispatch()
    const history = useHistory()

    return (
        <div className={styles.navbar}>
            <img className={styles.logo} src={logo} alt="logo" />
            <div className={styles.logout}>
                <div className={styles.text}>{`Welcome ${email}!`}</div>
                <MyButton onClick={event => {
                    dispatch(logout())
                    event.preventDefault()
                    history.push('/login')
                }} style={{ "margin": "0 40px 0 50px" }}>LOGOUT</MyButton>
            </div>
        </div>
    )
}

export default Header