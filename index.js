const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');
const corsMiddleware = require('./middleware/cors.middleware');

const PORT = 8080;
const {mongoDb} = require('./config/config');

const app = express();
app.use(express.json());
app.use(corsMiddleware);
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    mongoose.connect(mongoDb);
    app.listen(PORT, () => {
      console.log('Server START on port', PORT);
    });
  } catch (error) {

  }
};

start();
