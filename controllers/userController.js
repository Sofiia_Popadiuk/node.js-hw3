const User = require('../models/User');
const bcrypt = require('bcrypt');

class UserController {
  async getProfile(req, res) {
    try {
      const user = await User.findOne({_id: req.user.id});
      console.log('Get profile info');
      res.status(200).send({user: {_id: user.id, role: user.role, email: user.email, created_date: user.createDate}});
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async delete(req, res) {
    try {
      if (await User.findOneAndDelete({_id: req.user.id})) {
        console.log('User deleted success');
        return res.status(200).send({message: 'Profile deleted successfully'});
      } else {
        console.log('No user with this id');
        return res.status(400).send({message: 'Error message'});
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async changePassword(req, res) {
    try {
      const body = req.body;
      const user = await User.findOne({_id: req.user.id});
      const validOldPassword = await bcrypt.compare(body.oldPassword, user.password);
      if (!validOldPassword) {
        res.status(400).send({message: 'Error message'});
      } else {
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(body.newPassword, salt);
        await User.findOneAndUpdate({_id: req.user.id}, {password: hashPassword});
        console.log('Password changed success');
        res.status(200).send({message: 'Password changed successfully'});
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }
}

module.exports = new UserController();
