const Load = require('../models/Load')
const Truck = require('../models/Truck')
const User = require('../models/User')
const truckTypes = require('../truckTypes')

class LoadController {
    async getLoads(req, res) {
        try {
            const user = await User.findOne({ _id: req.user.id })
            let limit = 10;
            if (req.query.limit) {
                limit = req.query.limit > 50 ? 50 : +req.query.limit
            }

            if (user.role === 'DRIVER') {
                await Load
                    .find({ assigned_to: req.user.id })
                    .skip(+req.query.offset || 0)
                    .limit(limit)
                    .select('-__v')
                    .then(allLoads => {
                        return res.status(200).send({ loads: allLoads })
                    })
                    .catch(error => {
                        console.error(error)
                        res.status(400).send({ message: "Error message" })
                    })
            } else {
                await Load
                    .find({ created_by: req.user.id})
                    .skip(+req.query.offset || 0)
                    .limit(limit)
                    .select('-__v')
                    .then(allLoads => {
                        return res.status(200).send({ loads: allLoads })
                    })
                    .catch(error => {
                        console.error(error)
                        res.status(400).send({ message: "Error message" })
                    })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async addLoad(req, res) {
        try {
            const load = new Load({
                name: req.body.name,
                payload: req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                created_by: req.user.id,
                logs: [],
                assigned_to: null,
                status: "NEW",
                state: '',
                dimensions: req.body.dimensions,
                created_date: new Date()
            })
            await load.save()
            console.log('Load created')

            return res.status(200).send({ message: 'Load created successfully' })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async getActiveLoad(req, res) {
        try {
            await Load.findOne({ assigned_to: req.user.id, status: 'ASSIGNED' }).select('-__v')
                .then(load => {
                    console.log('Load find success')
                    return res.status(200).send({ load: load })
                })
                .catch(error => {
                    console.log('This drive have not load')
                    res.status(400).send({ message: "Error message" })
                })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async iterateToNextLoadState(req, res) {
        let newState = '';
        try {
            await Load.findOne({ assigned_to: req.user.id, status: 'ASSIGNED' })
                .then(load => {
                    if (load.state === "En route to Pick Up") {
                        newState = "Arrived to Pick Up"
                        load.state = "Arrived to Pick Up"
                    } else if (load.state === "Arrived to Pick Up") {
                        newState = "En route to delivery"
                        load.state = "En route to delivery"
                    } else {
                        newState = "Arrived to delivery"
                        load.state = "Arrived to delivery"
                        load.status = 'SHIPPED'
                    }
                    load.save()
                })
            if (newState === "Arrived to delivery") {
                await Truck.findOneAndUpdate({ assigned_to: req.user.id }, { status: 'IS' })
            }

            return res.status(200).send({ message: "Load state changed to 'En route to Delivery'" })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    //перевіряти чи лоад даного користувача
    async getLoadById(req, res) {
        try {
            await Load.findOne({ _id: req.params.id }).select('-__v')
                .then(load => {
                    console.log('Load get success')
                    return res.status(200).send({ load: load })
                })
                .catch(error => {
                    console.log('No load with this id')
                    return res.status(400).send({ message: "Error message" })
                })
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async updateLoadById(req, res) {
        try {
            if (await Load.findByIdAndUpdate({ _id: req.params.id }, {
                name: req.body.name,
                payload: req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                dimensions: req.body.dimensions
            })) {
                console.log('Load update success')
                return res.status(200).send({ message: 'Load details changed successfully' })
            } else {
                console.log('No load with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async deleteLoadById(req, res) {
        try {
            if (await Load.findOneAndDelete({ _id: req.params.id })) {
                console.log('Load deleted success')
                return res.status(200).send({ message: 'Load deleted successfully' })
            } else {
                console.log('No load with this id')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async postLoadById(req, res) {
        try {
            const load = await Load.findOne({ _id: req.params.id })
            if (load.status === 'NEW') {
                await Load.findOneAndUpdate({ _id: req.params.id }, { status: 'POSTED' })
                load.logs.push({
                    message: 'This load is <POSTED>',
                    time: new Date()
                });
                const trucksIS = await Truck.find({ status: 'IS', assigned_to: { $ne: null } })
                if (trucksIS.length === 0) {
                    await Load.findOneAndUpdate({ _id: req.params.id }, { status: 'NEW' })
                    load.logs.push({
                        message: 'No truck with assigned driver. Status rolled back to <NEW>',
                        time: new Date()
                    });
                    load.save();
                    res.status(200).send({ message: "No truck with assigned driver. Status rolled back to <NEW>", driver_found: false })
                } else {
                    let bestTruck;
                    let check = true;
                    trucksIS.forEach(truck => {
                        const dimensions = truckTypes[truck.type].dimensions
                        const payload = truckTypes[truck.type].payload
                        if (
                            check &&
                            dimensions.width > load.dimensions.width &&
                            dimensions.length > load.dimensions.length &&
                            dimensions.height > load.dimensions.height &&
                            payload > load.payload
                        ) {
                            bestTruck = truck
                            check === false
                        }
                    })
                    if (bestTruck) {
                        await Truck.findOneAndUpdate({ _id: bestTruck._id }, { status: 'OL' })
                        await Load.findOneAndUpdate({ _id: req.params.id }, {
                            status: 'ASSIGNED',
                            state: 'En route to Pick Up',
                            assigned_to: bestTruck.assigned_to
                        })
                        load.logs.push({
                            message: `Load assigned to driver with id ${bestTruck.assigned_to}`,
                            time: new Date()
                        });
                        res.status(200).send({ message: "Load posted successfully", driver_found: true })
                    } else {
                        await Load.findOneAndUpdate({ _id: req.params.id }, { status: 'NEW' })
                        load.logs.push({
                            message: 'No truck with needed parametres. Status return to <NEW>',
                            time: new Date()
                        });
                        res.status(200).send({ message: "Load posted successfully", driver_found: false })
                    }
                    load.save();
                }
            } else {
                res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }

    async getLoadShippingInfoById(req, res) {
        try {
            const loadById = await Load.findOne({ _id: req.params.id, status: 'ASSIGNED' }).select('-__v')
            if (loadById) {
                await Truck.findOne({ assigned_to: loadById.assigned_to }).select('-__v')
                    .then(truck => {
                        return res.status(200).send({ load: loadById, truck: truck })
                    })
                    .catch(error => {
                        return res.status(400).send({ message: "Error message" })
                    })
            } else {
                console.log('No load with this id or this delivery finished')
                return res.status(400).send({ message: "Error message" })
            }
        } catch (error) {
            console.error(error)
            res.status(400).send({ message: "Error message" })
        }
    }
}

module.exports = new LoadController()