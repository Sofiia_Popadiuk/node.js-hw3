const Truck = require('../models/Truck');
const bcrypt = require('bcrypt');

class TruckController {
  async getTrucks(req, res) {
    try {
      const allTrucks = await Truck.find({created_by: req.user.id}).select('-__v');
      return res.status(200).send({
        trucks: allTrucks,
      });

      // await Truck.find({ created_by: req.user.id }).select('-__v')
      //     .then(trucks => {
      //         res.status(200).send({
      //             trucks: trucks
      //         })
      //     })
      //     .catch(error => console.error(error))
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async postTruck(req, res) {
    try {
      const truck = new Truck({
        created_by: req.user.id,
        assigned_to: null,
        type: req.body.type,
        status: 'IS',
        created_date: new Date(),
      });
      await truck.save();
      console.log('Truck created');

      return res.status(200).send({message: 'Truck created successfully'});
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async getTruckById(req, res) {
    try {
      const truck = await Truck.findOne({_id: req.params.id}).select('-__v');
      if (truck) {
        console.log('Get truck by id success');
        return res.status(200).send({truck: truck});
      } else {
        console.log('No truck with this id');
        return res.status(400).send({message: 'Error message'});
      }

      // await Truck.findOne({ _id: req.params.id }).select('-__v')
      //     .then(truck => {
      //         res.status(200).send({
      //             truck: truck
      //         })
      //     })
      //     .catch(error => res.status(400).send({ message: "Error message" }))
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async updateTruckById(req, res) {
    try {
      if (await Truck.findOneAndUpdate({_id: req.params.id}, {type: req.body.type})) {
        console.log('Truck update success');
        return res.status(200).send({message: 'Truck details changed successfully'});
      } else {
        console.log('No truck with this id');
        return res.status(400).send({message: 'Error message'});
      }

      // await Truck.findOneAndUpdate({ _id: req.params.id }, { type: req.body.type })
      //     .then(truck => {
      //         res.status(200).send({ message: 'Truck details changed successfully' })
      //     })
      //     .catch(error => res.status(400).send({ message: "Error message" }))
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async deleteTruckById(req, res) {
    try {
      if (await Truck.findOneAndDelete({_id: req.params.id})) {
        console.log('Truck deleted success');
        return res.status(200).send({message: 'Truck deleted successfully'});
      } else {
        console.log('No truck with this id');
        return res.status(400).send({message: 'Error message'});
      }

      // await Truck.findOneAndDelete({ _id: req.params.id })
      //     .then(truck => {
      //         res.status(200).send({ message: 'Truck deleted successfully' })
      //     })
      //     .catch(error => res.status(400).send({ message: "Error message" }))
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async assignTruckToUser(req, res) {
    try {
      if (await Truck.findOne({assigned_to: req.user.id}) === null) {
        if (await Truck.findOneAndUpdate({_id: req.params.id}, {assigned_to: req.user.id})) {
          console.log('Truck assigned success');
          return res.status(200).send({message: 'Truck assigned successfully'});
        } else {
          console.log('No truck with this id');
          return res.status(400).send({message: 'Error message'});
        }
      } else {
        console.log('User is already assigned');
        return res.status(400).send({message: 'Error message'});
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }
}

module.exports = new TruckController();
