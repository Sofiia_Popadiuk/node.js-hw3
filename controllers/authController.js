const bcrypt = require('bcrypt');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');

const {secret} = require('../config/config');
const mailer = require('../nodemailer');

const generateToken = (id, email) => {
  const payload = {id, email};
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

const generatePassword = () => {
  const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  let password = '';
  for (let i = 0; i < 10; i++) {
    const check = Math.floor(Math.random() * 2);
    if (check === 0) {
      const numb = Math.floor(Math.random() * 10);
      password += numb;
    } else {
      const numb = Math.floor(Math.random() * alphabet.length);
      password += alphabet[numb];
    }
  }
  return password;
};

class AuthController {
  async registration(req, res) {
    try {
      const body = req.body;

      if (!validationResult(req).isEmpty()) {
        return res.status(400).send('Error message');
      }

      if (await User.findOne({email: body.email})) {
        return res.status(400).send('Error message');
      }

      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(body.password, salt);
      const user = new User({email: body.email, password: hashPassword, createDate: new Date(), role: body.role});
      await user.save();
      console.log('User created');

      return res.status(200).send({message: 'Profile created successfully'});
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async login(req, res) {
    try {
      const body = req.body;

      if (!validationResult(req).isEmpty()) {
        return res.status(400).send('Error message');
      }

      const user = await User.findOne({email: body.email});
      if (user) {
        const validPassword = await bcrypt.compare(body.password, user.password);
        if (validPassword) {
          const token = generateToken(user._id, user.email);
          console.log('Login success');
          return res.status(200).send({jwt_token: token});
        } else {
          console.log('Password is invalid');
          return res.status(400).send({message: 'Error message'});
        }
      } else {
        res.status(400).send({message: 'Error message'});
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }

  async forgotPassword(req, res) {
    try {
      const email = req.body.email;
      const password = generatePassword();
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(password, salt);

      await User.findOneAndUpdate({email: email}, {password: hashPassword})
          .then(() => {
            const message = {
              to: email,
              subject: 'Change password',
              text: `Your new password for ${email} - ${password}`,
            };
            mailer(message);

            console.log('Password changed');
            return res.status(200).send({message: 'New password sent to your email address'});
          })
          .catch((error) => {
            console.error(error);
            res.status(400).send({message: 'Error message'});
          });
    } catch (error) {
      console.error(error);
      res.status(400).send({message: 'Error message'});
    }
  }
}

module.exports = new AuthController();
