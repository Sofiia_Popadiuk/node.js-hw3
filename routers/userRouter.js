const Router = require('express');
const router = new Router();
const controller = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, controller.getProfile);
router.delete('/me', authMiddleware, controller.delete);
router.patch('/me/password', authMiddleware, controller.changePassword);

module.exports = router;
