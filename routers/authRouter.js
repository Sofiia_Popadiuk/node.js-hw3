const Router = require('express');
const router = new Router();
const controller = require('../controllers/authController');
const {check} = require('express-validator');

router.post('/register', [
  check('email', 'Uncorrect email').isEmail(),
  check('password', 'Password must be more than 3 and less than 12 symbols').isLength({min: 3, max: 12}),
  check('role', 'Role can not be empty').notEmpty(),
], controller.registration);
router.post('/login', [
  check('email', 'Uncorrect email').isEmail(),
  check('password', 'Password must be more than 3 and less than 12 symbols').isLength({min: 3, max: 12}),
], controller.login);
router.post('/forgot_password', controller.forgotPassword);

module.exports = router;
