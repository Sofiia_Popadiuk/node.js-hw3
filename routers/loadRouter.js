const Router = require('express');
const router = new Router();
const controller = require('../controllers/loadController');
const authMiddleware = require('../middleware/authMiddleware');
const driverMiddleware = require('../middleware/driverMiddleware');
const shipperMiddleware = require('../middleware/shipperMiddleware');

router.get('/', authMiddleware, controller.getLoads);
router.post('/', authMiddleware, shipperMiddleware, controller.addLoad);
router.get('/active', authMiddleware, driverMiddleware, controller.getActiveLoad);
router.patch('/active/state', authMiddleware, driverMiddleware, controller.iterateToNextLoadState);
router.get('/:id', authMiddleware, controller.getLoadById);
router.put('/:id', authMiddleware, shipperMiddleware, controller.updateLoadById);
router.delete('/:id', authMiddleware, shipperMiddleware, controller.deleteLoadById);
router.post('/:id/post', authMiddleware, shipperMiddleware, controller.postLoadById);
router.get('/:id/shipping_info', authMiddleware, shipperMiddleware, controller.getLoadShippingInfoById);

module.exports = router;
