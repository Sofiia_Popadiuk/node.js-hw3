const Router = require('express');
const router = new Router();
const controller = require('../controllers/truckController');
const authMiddleware = require('../middleware/authMiddleware');
const driverMiddleware = require('../middleware/driverMiddleware');

router.get('/', authMiddleware, driverMiddleware, controller.getTrucks);
router.post('/', authMiddleware, driverMiddleware, controller.postTruck);
router.get('/:id', authMiddleware, driverMiddleware, controller.getTruckById);
router.put('/:id', authMiddleware, driverMiddleware, controller.updateTruckById);
router.delete('/:id', authMiddleware, driverMiddleware, controller.deleteTruckById);
router.post('/:id/assign', authMiddleware, driverMiddleware, controller.assignTruckToUser);

module.exports = router;
